User Environment
----------------

APT_CONFIG=/var/lib/sbuild/apt.conf
DEB_BUILD_OPTIONS=parallel=4
HOME=/sbuild-nonexistent
LANG=C.UTF-8
LC_ALL=C.UTF-8
LOGNAME=buildd
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
SCHROOT_ALIAS_NAME=build-PACKAGEBUILD-19871030
SCHROOT_CHROOT_NAME=build-PACKAGEBUILD-19871030
SCHROOT_COMMAND=env
SCHROOT_GID=2501
SCHROOT_GROUP=buildd
SCHROOT_SESSION_ID=build-PACKAGEBUILD-19871030
SCHROOT_UID=2001
SCHROOT_USER=buildd
SHELL=/bin/sh
TERM=unknown
USER=buildd
V=1

dpkg-buildpackage
-----------------

dpkg-buildpackage: info: source package postgresql-common
dpkg-buildpackage: info: source version 214ubuntu0.1
dpkg-buildpackage: info: source distribution focal-security
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 fakeroot debian/rules clean
dh clean --with systemd
   dh_auto_clean
	make -j1 clean
make[1]: Entering directory '/<<PKGBUILDDIR>>'
rm -f *.1 *.8 dh_make_pgxs/*.1
make[1]: Leaving directory '/<<PKGBUILDDIR>>'
   dh_clean
 debian/rules build
dh build --with systemd
   dh_update_autotools_config
   debian/rules override_dh_auto_configure
make[1]: Entering directory '/<<PKGBUILDDIR>>'
### Building postgresql-common flavor default
### Supported PostgreSQL versions: 12 (default version: 12)
make[1]: Leaving directory '/<<PKGBUILDDIR>>'
   dh_auto_build
	make -j1
make[1]: Entering directory '/<<PKGBUILDDIR>>'
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 pg_conftool pg_conftool.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 pg_createcluster pg_createcluster.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 pg_ctlcluster pg_ctlcluster.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 pg_dropcluster pg_dropcluster.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 pg_lsclusters pg_lsclusters.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 pg_renamecluster pg_renamecluster.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 pg_upgradecluster pg_upgradecluster.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 pg_wrapper pg_wrapper.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 pg_buildext.pod pg_buildext.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 pg_virtualenv.pod pg_virtualenv.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 1 dh_make_pgxs/dh_make_pgxs.pod dh_make_pgxs/dh_make_pgxs.1
pod2man --center "Debian PostgreSQL infrastructure" -r "Debian" --quotes=none --section 8 pg_updatedicts pg_updatedicts.8
make[1]: Leaving directory '/<<PKGBUILDDIR>>'
   dh_auto_test
 fakeroot debian/rules binary
dh binary --with systemd
   dh_testroot
   dh_prep
   dh_installdirs
   dh_auto_install
   debian/rules override_dh_install
make[1]: Entering directory '/<<PKGBUILDDIR>>'
dh_install
/usr/bin/make -C systemd install DESTDIR=/<<PKGBUILDDIR>>/debian/postgresql-common
make[2]: Entering directory '/<<PKGBUILDDIR>>/systemd'
install -d /<<PKGBUILDDIR>>/debian/postgresql-common/lib/systemd/system-generators/ /<<PKGBUILDDIR>>/debian/postgresql-common/lib/systemd/system/
install postgresql-generator /<<PKGBUILDDIR>>/debian/postgresql-common/lib/systemd/system-generators/
install -m644 postgresql*.service /<<PKGBUILDDIR>>/debian/postgresql-common/lib/systemd/system/
make[2]: Leaving directory '/<<PKGBUILDDIR>>/systemd'
install -m 644 -D debian/postgresql-common.sysctl debian/postgresql-common/etc/sysctl.d/30-postgresql-shm.conf
/bin/echo -e "# See /usr/share/postgresql-common/supported-versions for documentation of this file\ndefault" > debian/postgresql-client-common/etc/postgresql-common/supported_versions
make[1]: Leaving directory '/<<PKGBUILDDIR>>'
   dh_installdocs
   dh_installchangelogs
   dh_installman
   dh_installdebconf
   dh_systemd_enable
   debian/rules override_dh_installinit
make[1]: Entering directory '/<<PKGBUILDDIR>>'
dh_installinit -ppostgresql-common --name=postgresql -u'defaults 19 21' -r
make[1]: Leaving directory '/<<PKGBUILDDIR>>'
   dh_systemd_start
   dh_installlogrotate
   dh_lintian
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   debian/rules override_dh_gencontrol
make[1]: Entering directory '/<<PKGBUILDDIR>>'
dh_gencontrol -ppostgresql-server-dev-all -- -Vserver-dev-all-depends="postgresql-server-dev-12,"
# the versionless metapackages need to have version numbers which match
# the server version, not the p-common version
dh_gencontrol -ppostgresql -ppostgresql-client -ppostgresql-doc -ppostgresql-contrib -ppostgresql-all -- \
	-Vdefault-version="12" -v'12+${source:Version}' \
	-Vpostgresql-all-depends="postgresql-contrib-12,postgresql-plperl-12,,postgresql-plpython3-12,postgresql-pltcl-12,"
dh_gencontrol -ppostgresql-client-common -- -Vpgdg:Depends="" -Vreadline:Recommends=libreadline8
dpkg-gencontrol: warning: package postgresql-client-common: substitution variable ${perl:Depends} unused, but is defined
dh_gencontrol --remaining-packages
dpkg-gencontrol: warning: package postgresql-common: substitution variable ${perl:Depends} unused, but is defined
make[1]: Leaving directory '/<<PKGBUILDDIR>>'
   dh_md5sums
   dh_builddeb
INFO: pkgstriptranslations version 144
INFO: pkgstriptranslations version 144
INFO: pkgstriptranslations version 144
INFO: pkgstriptranslations version 144
pkgstriptranslations: processing postgresql-server-dev-all (in debian/postgresql-server-dev-all); do_strip: 1, oemstrip: 
pkgstriptranslations: processing postgresql-common (in debian/postgresql-common); do_strip: 1, oemstrip: 
pkgstriptranslations: processing postgresql-contrib (in debian/postgresql-contrib); do_strip: 1, oemstrip: 
pkgstriptranslations: processing postgresql-client (in debian/postgresql-client); do_strip: 1, oemstrip: 
pkgstriptranslations: postgresql-server-dev-all does not contain translations, skipping
pkgstriptranslations: preparing translation tarball postgresql-common_214ubuntu0.1_amd64_translations.tar.gz...done
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/postgresql-server-dev-all/DEBIAN/control, package postgresql-server-dev-all, directory debian/postgresql-server-dev-all
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
pkgstriptranslations: postgresql-common does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgstriptranslations: postgresql-contrib does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgstriptranslations: postgresql-client does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/postgresql-common/DEBIAN/control, package postgresql-common, directory debian/postgresql-common
Searching for duplicated docs in dependency postgresql-client-common...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
  symlinking changelog.gz in postgresql-common to file in postgresql-client-common
pkgstripfiles: processing control file: debian/postgresql-contrib/DEBIAN/control, package postgresql-contrib, directory debian/postgresql-contrib
pkgstripfiles: Running PNG optimization (using 4 cpus) for package postgresql-common ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
pkgstripfiles: processing control file: debian/postgresql-client/DEBIAN/control, package postgresql-client, directory debian/postgresql-client
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
o
pkgstripfiles: PNG optimization (1/0) for package postgresql-common took 14 s
dpkg-deb: building package 'postgresql-common' in '../postgresql-common_214ubuntu0.1_all.deb'.
INFO: pkgstripfiles: waiting for lock (postgresql-server-dev-all) ...
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstriptranslations version 144
pkgstriptranslations: processing postgresql-client-common (in debian/postgresql-client-common); do_strip: 1, oemstrip: 
pkgstriptranslations: postgresql-client-common does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/postgresql-client-common/DEBIAN/control, package postgresql-client-common, directory debian/postgresql-client-common
pkgstripfiles: Truncating usr/share/doc/postgresql-client-common/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package postgresql-client-common ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'postgresql-client-common' in '../postgresql-client-common_214ubuntu0.1_all.deb'.
Searching for duplicated docs in dependency postgresql-common...
  symlinking changelog.gz in postgresql-server-dev-all to file in postgresql-client-common
pkgstripfiles: Running PNG optimization (using 4 cpus) for package postgresql-server-dev-all ...
pkgstripfiles: No PNG files.
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
dpkg-deb: building package 'postgresql-server-dev-all' in '../postgresql-server-dev-all_214ubuntu0.1_all.deb'.
INFO: pkgstripfiles: waiting for lock (postgresql-client) ...
INFO: pkgstriptranslations version 144
pkgstriptranslations: processing postgresql (in debian/postgresql); do_strip: 1, oemstrip: 
pkgstriptranslations: postgresql does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/postgresql/DEBIAN/control, package postgresql, directory debian/postgresql
pkgstripfiles: Truncating usr/share/doc/postgresql/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package postgresql ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'postgresql' in '../postgresql_12+214ubuntu0.1_all.deb'.
INFO: pkgstripfiles: waiting for lock (postgresql-contrib) ...
pkgstripfiles: Truncating usr/share/doc/postgresql-client/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package postgresql-client ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'postgresql-client' in '../postgresql-client_12+214ubuntu0.1_all.deb'.
INFO: pkgstriptranslations version 144
pkgstriptranslations: processing postgresql-doc (in debian/postgresql-doc); do_strip: 1, oemstrip: 
pkgstriptranslations: postgresql-doc does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/postgresql-doc/DEBIAN/control, package postgresql-doc, directory debian/postgresql-doc
pkgstripfiles: Truncating usr/share/doc/postgresql-doc/changelog.gz to topmost ten records
pkgstripfiles: Disabled PNG optimization for -doc package postgresql-doc (to save build time)
dpkg-deb: building package 'postgresql-doc' in '../postgresql-doc_12+214ubuntu0.1_all.deb'.
pkgstripfiles: Truncating usr/share/doc/postgresql-contrib/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package postgresql-contrib ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'postgresql-contrib' in '../postgresql-contrib_12+214ubuntu0.1_all.deb'.
INFO: pkgstriptranslations version 144
pkgstriptranslations: processing postgresql-all (in debian/postgresql-all); do_strip: 1, oemstrip: 
pkgstriptranslations: postgresql-all does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/postgresql-all/DEBIAN/control, package postgresql-all, directory debian/postgresql-all
Searching for duplicated docs in dependency postgresql-server-dev-all...
  symlinking changelog.gz in postgresql-all to file in postgresql-client-common
pkgstripfiles: Running PNG optimization (using 4 cpus) for package postgresql-all ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'postgresql-all' in '../postgresql-all_12+214ubuntu0.1_all.deb'.
 dpkg-genbuildinfo --build=binary
 dpkg-genchanges --build=binary -mLaunchpad Build Daemon <buildd@lgw01-amd64-038.buildd> >../postgresql-common_214ubuntu0.1_amd64.changes
dpkg-genchanges: info: binary-only upload (no source code included)
 dpkg-source --after-build .
